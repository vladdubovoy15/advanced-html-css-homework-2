"use strict";

const menuBurger = document.querySelector(".menu-burger");

menuBurger.onclick = function(){
    menuBurger.classList.toggle("menu-burger--open");
    document.querySelector(".navigation__list").classList.toggle("navigation__list--open"); 
}