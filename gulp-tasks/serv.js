const browserSync = require("browser-sync").create();

const serv = () => {
	browserSync.init({
		server: {
			baseDir: "./",
		},
		browser: "safari",
	});
};
exports.browsersync = browserSync;
exports.serv = serv;
